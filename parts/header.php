<header class="header" role="banner">
    <nav class="secondary-menu navbar navbar-expand-lg navbar-dark">
        <div class="container">
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Testimonials</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Case Studies</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Jenman</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Careers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="primary-menu navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a class="navbar-brand" href="#"><img class="logo" alt="TG Newton Logo" src="img/TG%20Newton_Logo.PNG"/></a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sell</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Buy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Rent</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-phone" aria-hidden="true"></i>(03) 9568 8000</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <nav class="mobile-menu navbar navbar-expand-lg navbar-light">
        <div class="container">
            <div class="mobile-header">
                <a class="menu-mobile" href="#">Menu</a>
                <a class="navbar-brand" href="#"><img class="logo" alt="TG Newton Logo" src="img/TG%20Newton_Logo.PNG"/></a>
                <div class="icons-right">
                    <a class="search-mobile" href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
                    <a class="call-mobile" href="#"><i class="fa fa-phone" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

        <div class="mobile-menu-wrap">
            <div class="mobile-header">
                <a class="close-mobile" href="#">Close</a>
                <span>Menu</span>
            </div>

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Sell</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Buy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Rent</a>
                </li>
                <li class="nav-item">
                    <span class="nav-separator"></span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Testimonials</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Case Studies</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Jenman</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Careers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                <li class="nav-item contact-cta">
                    <a class="nav-link" href="#"><i class="fa fa-phone" aria-hidden="true"></i>(03) 9568 8000</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class=""></div>

</header>