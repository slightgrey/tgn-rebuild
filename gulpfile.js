//* Define plugins
var gulp = require('gulp'),
watch = require('gulp-watch'),
browserSync = require('browser-sync').create();
var $ = require('gulp-load-plugins')();

//* Define paths
var paths = {
    cssFiles: './css/**/*.css',
    cssDirectory: './css/',
    sassFiles: './scss/**/*.scss',
    sassDirectory: './scss/'
};


//* Gulp Watch

gulp.task('watch', function() {

    browserSync.init({
      notify: false,
      server: true
    });
  
    watch('*.html', function() {
      browserSync.reload();
    });

    // watch(paths.sassFiles, function() {
    //     gulp.start('cssInject');
    //   });
  
    watch(paths.sassDirectory, function() {
      gulp.start('cssInject');
    });
  
  });

gulp.task('cssInject', ['compile-sass'], function() {
    return gulp.src(paths.cssDirectory+'main.css')
      .pipe(browserSync.stream());
  });



//* Compile SASS to CSS
gulp.task('compile-sass', function () {
    return gulp.src(paths.sassDirectory+'main.scss')
		.pipe($.plumber(function(error) {
            $.util.log(error.message);
            this.emit('end');
        }))
        .pipe($.sass())
        .pipe($.autoprefixer({browsers: ['last 2 versions', 'Explorer >= 9', 'Android >= 4.1', 'Safari >= 6', 'iOS >= 7', 'Firefox > 18', 'Opera > 17']}))
        .pipe(gulp.dest(paths.cssDirectory));
});

//* Watchers
// gulp.task('default', function () {
//     gulp.watch(paths.sassFiles, ['compile-sass']);
// });

